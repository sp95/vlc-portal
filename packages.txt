Click==7.0
Django==1.11.18
Flask==1.0.2
image==1.5.27
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.0
numpy==1.15.2
Pillow==5.4.1
pypng==0.0.19
PyQRCode==1.2.1
pytz==2018.9
qrcode==6.1
six==1.12.0
Werkzeug==0.14.1
zbar==0.10
