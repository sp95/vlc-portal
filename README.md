# vlc-portal

A VLC portal including various applications.

To remove cache and not store cache:
  rm -rf ~/.cache/google-chrome
  ln -s /dev/null ~/.cache/google-chrome

opencv (cv2) version required [for multithreading support]: 2.4.9.1 [No more available in pypi]
numpy version: 1.15.2

zbar installation:
  sudo apt-get install libzbar-dev
  pip install zbar

To install the dependencies:
  pip install -r packages.txt

To run the application:
  python main.py

In the browser:
  localhost:5000/
