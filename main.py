from flask import Flask, request, render_template, redirect, url_for
import send_colors
import send_qr
import base64

import os
import json
import pyqrcode
import qrcode
import cv2
import threading
import zbar
from PIL import Image, ImageDraw

app = Flask(__name__)

@app.route('/')
def index():
   return render_template('login.html')


@app.route('/login', methods=['POST'])
def login():
    print("*"*20, "LOGIN INFO", "*"*20)
    name = request.form['name']
    password = request.form['password']
    print("Name: ", name, "Password: ", password)
    print("*"*60)
    return render_template('portal.html')

@app.route('/transmit', methods=['POST', 'GET'])
def transmit_message():
    global data
    tx_type = request.form['txtype']
    if tx_type == "color":
        if data == "":
            print("***********DATA IS EMPTY****************")
            pass
            #send_colors.send_data("hello brother")
        else:
            send_colors.send_data(data)
    else:
        if data == "":
            print("***********DATA IS EMPTY****************")
            pass
            #send_qr.send_text("hello brother");
        else:
            send_qr.send_text(data);
    return "transmit response"

@app.route('/typed', methods=['POST', 'GET'])
def typed_message():
    global data
    typed = request.form['typed']
    print("Typed>>> ", typed)
    data = str(typed)
    print("Data>>> ", data)
    return "typed response"

@app.route('/upload', methods=['POST', 'GET'])
def upload_doc():
    global data
    print("*"*20, "UPLOADED", "*"*20)
    datatype = request.form['datatype']
    filepath = request.files['file']
    print("Datatype: ", datatype, "filepath: ",filepath)
    if datatype == 'Image':
        data = base64.b64encode(filepath.read())
    elif datatype == 'Text':
        data = filepath.read()
    print("DATA>>>", data)
    print("*"*60)
    return "Uploaded Response"

@app.route('/chat', methods=['POST', 'GET'])
def render_chat():
    print("Came to chat>>>")
    return render_template('chat.html')

@app.route('/logout', methods=['POST', 'GET'])
def logout():
    print("Came to logout>>>")
    return render_template('portal.html')

@app.route("/script", methods=['POST'])
def script():
    input_string = request.form['data']
    if(input_string=="SendingImage"):
    	os.system('espeak "Capturing"')
	'''
	while(True):
		if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
		ret, frame = capture.read()
		cv2.imshow('frame', frame)
		#cv2.waitKey(0)
        cv2.destroyAllWindows()
	'''
	ret1, pic = capture.read()
	temp_pic = pic
	cv2.imshow('pic', temp_pic)
        cv2.waitKey(500)
        cv2.destroyAllWindows()
	pic = cv2.resize(pic, (50, 50))
	send_pic(pic)
	#cv2.imshow('pic', pic)
        #cv2.waitKey(2000)
    else:
    	create_qr(input_string)
    cv2.destroyAllWindows()
    print input_string
    return "backend response"

@app.route('/getpythondata')
def get_python_data():
    #pythondata={'Message': 'hello there'}
    if(len(sendList)!=0):
    	pythondata = sendList[0]
	del sendList[:]
    else:
    	pythondata = ""
    del sendList[:]

    return str(pythondata)

def decode_img():
        list2 = [1,]
        while True:
                # To quit this program press q.
                if cv2.waitKey(1) & 0xFF == ord('q'):
                        break

                # Breaks down the video into frames
                ret, frame = capture.read()

                # Displays the current frame
                #cv2.imshow('Current', frame)

                # Converts image to grayscale.
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Uses PIL to convert the grayscale image into a ndary array that ZBar can understand.
                image = Image.fromarray(gray)
                width, height = image.size
                zbar_image = zbar.Image(width, height, 'Y800', image.tobytes())

                # Scans the zbar image.
                scanner = zbar.ImageScanner()
                scanner.scan(zbar_image)
                for decoded in zbar_image:
			if decoded.data in list2:
                                 pass
                        else:
                                if decoded.data=='start_img':
                                        list2.pop()
                                        print("Shailesh : Start of frame")
                                        print(decoded.data)
                                        list2.append(decoded.data)
                                elif decoded.data=='end_img':
                                        list2.pop()
                                        list2.append(decoded.data)
                                        print("Shailesh: Frame Ended")
                                        print(decoded.data)
                                        return fileName
                                else:
					list2.pop()
                                        print(decoded.data)
                                        img_data = decoded.data
                                        list2.append(decoded.data)
					fileName = os.getcwd()
					fileName = fileName + "/qrDecoded" + str(imgCount) + ".png"
                                        with open(fileName, 'a+b') as f:
                                                #f.write(binascii.unhexlify(decoded.data))
                                                f.write(img_data.decode('base64'))
                                        #f.write(decoded.data)
                                        #f.write('\n')

def decode():
    	global capture
	global sendList
	global imgCount
	imgCount = 0
	sendList = []
    	#capture = cv2.VideoCapture(0)
    	capture = cv2.VideoCapture('http://192.168.137.129:8080/video')
	list1 = [1,]
        while True:
                # To quit this program press q.
                if cv2.waitKey(1) & 0xFF == ord('q'):
                        break

                # Breaks down the video into frames
                ret, frame = capture.read()

                # Displays the current frame
                #cv2.imshow('Current', frame)

                # Converts image to grayscale.
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Uses PIL to convert the grayscale image into a ndary array that ZBar can understand.
                image = Image.fromarray(gray)
                width, height = image.size
                zbar_image = zbar.Image(width, height, 'Y800', image.tobytes())

                # Scans the zbar image.
                scanner = zbar.ImageScanner()
                scanner.scan(zbar_image)
		for decoded in zbar_image:
			if decoded.data in list1:
				pass
			else:
				if decoded.data=='start_img':
					os.system('spd-say "Image decoding started"')
                                 	fileName = decode_img()
					print(fileName)
					imgCount = imgCount + 1
				 	os.system('espeak "Image decoding ended"')
				 	decImg = cv2.imread(fileName)
				 	decImg = cv2.resize(decImg, (500, 500))
				 	decWritePath = os.getcwd()
				 	decWritePath = decWritePath + "/resized" + str(imgCount-1) + ".png"
				 	cv2.imwrite(decWritePath, decImg)
				 	cv2.imshow('decodedImg', decImg)
				 	cv2.waitKey(1000)
				 	cv2.destroyAllWindows()
  				elif decoded.data=='end_img':
  					#cv2.destroyAllWindows()
					pass
				else:
					list1.pop()
                        		print("Shailesh ", decoded.data)
                                	list1.append(decoded.data)
					sendList.append(decoded.data)
					temp = 'espeak "' +decoded.data+ '"'
					os.system(temp)


def save_qr(data):
    qr = pyqrcode.create(data)
    #print(type(qr))
    mypath = os.getcwd()
    #print((str(qr)))
    qr.png("pichorn.png", scale=6)
    mypath = mypath + '/pichorn.png'
    frame = cv2.imread(mypath)
    return frame

def make_qr(data):
    #qr = pyqrcode.create(data)
    qr = qrcode.make(data)
    qr.save('pichorn1.png')
    #print(type(qr))
    mypath = os.getcwd()
    #print((str(qr)))
    #qr.png("pichorn.png", scale=6)
    mypath = mypath + '/pichorn1.png'
    frame = cv2.imread(mypath)
    return frame

def display_qr(qr_list):
    #print("Starting to display")
    interval = int(500)
    for i in range(0,len(qr_list)):
        cv2.imshow('frame', qr_list[i])
        cv2.waitKey(interval)
    cv2.destroyAllWindows()

def send_pic(img):
    #cv2.destroyAllWindows()
    qr_list = []
    max_bits_cap = 100
    mypath = os.getcwd()
    mypath = mypath + '/pic.png'
    cv2.imwrite(mypath, img)
    os.system('espeak "Pre-Processing Started"')
    qr_list.append(make_qr('start_img'))
    with open(mypath, 'rb') as f:
    	content = f.read()
    	data = base64.b64encode(content)
        print("Length of data : ", len(data))
        iterate = int(len(data)/max_bits_cap)
	for j in range(0,iterate):
		temp_data = data[j*max_bits_cap:(j+1)*max_bits_cap]
		qr_list.append(make_qr(temp_data))
	temp_data = data[iterate*max_bits_cap:]
	qr_list.append(make_qr(temp_data))
    qr_list.append(make_qr('end_img'))
    os.system('espeak "Pre-Processing Completed"')
    display_qr(qr_list)
    #cv2.destroyAllWindows()

def create_qr(data):
    interval = int(500)
    qr = pyqrcode.create(data)
    qr.png("horn.png" ,scale =6)
    qr_path = os.getcwd()
    qr_path = qr_path + "/horn.png"
    frame = cv2.imread(qr_path)
    cv2.imshow('frame', frame)
    cv2.waitKey(interval)
    cv2.destroyAllWindows()


@app.route('/startdemo', methods=['POST', 'GET'])
def start_demo():
    #cv2.imshow('gauge-1', cv2.imread('factory/gauge1.png'))
    #cv2.waitKey(0)
    #cv2.imshow('gauge-2', cv2.imread('factory/gauge2.png'))
    #cv2.waitKey(0)
    #cv2.imshow('gauge-3', cv2.imread('factory/gauge3.png'))
    #cv2.waitKey(0)
    reading = "start_demo"
    #qr = pyqrcode.create(reading)
    #qr.png("factory/reading.png", scale=6)
    #frame = cv2.imread('factory/reading.png')
    #cv2.imshow('reading', frame)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    #cv2.destroyAllWindows()
    send_qr.send_reading(reading);
    return "Start Use Case done"

@app.route('/stopdemo', methods=['POST', 'GET'])
def end_demo():
    reading = "end_demo"
    send_qr.send_reading(reading);
    return "End Use Case done"


if __name__ == '__main__':
    global data
    sendList = []
    data = ""
    currentDir = os.getcwd()
    currentDir = currentDir + "/*.png"
    command = 'rm ' + currentDir
    os.system('touch test.png')
    os.system(command)
    t2 = threading.Thread(target=decode)
    t2.daemon = True
    t2.start()
    app.run()
