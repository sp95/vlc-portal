try:
    from Tkinter import Tk, Canvas #Python 2.x
except ImportError:
    from tkinter import Tk, Canvas #Python 3.x
import color_set


def append_white(d, key):
    s_new = ""
    for i in range(0, len(d), 32):
        if(len(d[i:i+32])==32):
            s_new = s_new + d[i:i+32] + key
        else:
            s_new = s_new + d[i:i+32]
    return s_new


def fec_encode(bit_stream, f):
    fec_code = ""
    for i in range(0, len(bit_stream), 2):
        fec_code = fec_code + f[bit_stream[i:i+2]]
        return fec_code

def make_grid(w,h,grid_dimension):
    grid_size = int((w)/grid_dimension)
    return grid_size

def set_border(cvs_w, cvs_h, border_w, canvas):
    canvas.create_rectangle(0,0,cvs_w,border_w,fill="red")
    canvas.create_rectangle(0,0,border_w,cvs_h,fill="red")
    canvas.create_rectangle((cvs_w-border_w),0,cvs_w,cvs_h,fill="red")
    canvas.create_rectangle(0,(cvs_h-border_w),cvs_w,cvs_h,fill="red")

def make_color(i,j,bits_start,bits_end, border_gap, matrix_dimension, data, canvas, bits, grid_size, colors):
    m = border_gap;
    n = border_gap;
    #set_border(500+2*border_gap,500+2*border_gap,10)
    for k in range(0,matrix_dimension*matrix_dimension):
        if len(data) < bits_end:
            canvas.create_rectangle(m,n,i,j,fill="white") #(width,height,x,y)
        else:
            temp_color = colors[data[bits_start:bits_end]]
            canvas.create_rectangle(m,n,i,j,fill=temp_color) #(width,height,x,y)
            #print(m, " ", n)
            bits_start=bits_start + bits
            bits_end=bits_end + bits
        if i != 500+border_gap:
            m = i
            i = m + 500/matrix_dimension #125
        else:
            n = j
            m = border_gap
            j = j + 500/matrix_dimension
            i = m + 500/matrix_dimension
    set_border(500+2*border_gap, 500+2*border_gap, 10, canvas)
    if len(data) > bits_end-bits:
        root.after(500, make_color,grid_size+border_gap,grid_size+border_gap,bits_start,bits_end, border_gap, matrix_dimension, data, canvas, bits, grid_size, colors) #colorslist, offset
    else:
        pass


def send_data(str_data):
    f = {}
    f['00'] = "00000" #red
    f['01'] = "11100" #black
    f['10'] = "10011" #blue
    f['11'] = "01111" #white

    global root
    root = Tk()
    root.title(" ")
    #root.iconbitmap("C:\\Users\\sh389892\\Desktop\\mypy\\OCC\\Final\\tx\\favicon.ico")
    data = ''.join(format(ord(i),'b').zfill(8) for i in str_data)
    #data = append_white(data, "1"*32)
    #data = '1'*(32*15) + data
    #fec_code = fec_encode(data, f)
    print("Length of Data: ", len(data))

    border_gap = 20
    colors, number_of_colors = color_set.get_color_set()
    #bits = int(log10(number_of_colors)/log10(2))
    bits = 2
    canvas_width, canvas_height, matrix_dimension = 500+2*border_gap, 500+2*border_gap, 4 #520 520
    grid_size = make_grid(canvas_width-2*border_gap, canvas_height-2*border_gap, matrix_dimension)
    canvas = Canvas(root,width=canvas_width,height=canvas_height)
    #set_border(500+border_gap,500+border_gap,10)
    make_color(grid_size+border_gap,grid_size+border_gap,0,bits, border_gap, matrix_dimension, data, canvas, bits, grid_size, colors)

    canvas.pack()
    root.mainloop()
