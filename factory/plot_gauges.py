import matplotlib.pyplot as plt
import cv2

g1 = cv2.imread('gauge-1.jpg')
g2 = cv2.imread('gauge-7.png')
g3 = cv2.imread('gauge-8.png')

g1_calib = cv2.imread('gauge-1-calibration.jpg')
g2_calib = cv2.imread('gauge-7-calibration.png')
g3_calib = cv2.imread('gauge-8-calibration.png')



fig = plt.figure()
a = fig.add_subplot(1, 2, 1)
imgplot = plt.imshow(g3)
plt.axis('off')
a.set_title('Gauge-3')

a = fig.add_subplot(1, 2, 2)
imgplot = plt.imshow(g3_calib)
plt.axis('off')
a.set_title('Gauge-3 Calibration')

plt.show()
