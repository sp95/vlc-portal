import pyqrcode
import cv2
import os

def save_qr(data):
    qr = pyqrcode.create(data)
    qr.png("horn.png", scale=6)
    dir = os.getcwd()
    frame = cv2.imread(dir + '/horn.png')
    return frame

def display_qr(qr_list):
    #print("Starting to display")
    print("Length of qr_list: ", len(qr_list))
    interval = int(500)
    for i in range(0,len(qr_list)):
        print("Came here for display ", i, type(qr_list[i]))
        cv2.imshow('frame', qr_list[i])
        cv2.waitKey(interval)
    cv2.destroyAllWindows()

def send_text(data):
    max_bits_cap = 500
    qr_list = []
    #sending start frame
    print("***************Pre-Processing Text Data**************")
    qr_list.append(save_qr('start_text'))
    #print(data)
    iterate = int(len(data)/max_bits_cap)
    for j in range(0,iterate):
        temp_data = data[j*max_bits_cap:(j+1)*max_bits_cap]
        qr_list.append(save_qr(temp_data))
    temp_data = data[iterate*max_bits_cap:]
    qr_list.append(save_qr(temp_data))

    qr_list.append(save_qr('end_text'))
    display_qr(qr_list)

def send_reading(data):
    max_bits_cap = 500
    qr_list = []
    #sending start frame
    print("***************Pre-Processing Text Data**************")
    #print(data)
    iterate = int(len(data)/max_bits_cap)
    for j in range(0,iterate):
        temp_data = data[j*max_bits_cap:(j+1)*max_bits_cap]
        qr_list.append(save_qr(temp_data))
    temp_data = data[iterate*max_bits_cap:]
    qr_list.append(save_qr(temp_data))
    display_qr(qr_list)
